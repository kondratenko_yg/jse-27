package ru.kondratenko.nlmk;

public interface IFactorial {
    int getFactorial(int f);
}
