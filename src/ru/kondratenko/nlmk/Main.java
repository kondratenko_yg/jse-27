package ru.kondratenko.nlmk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.logging.Logger;

public class Main {

    public static final Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        Factorial factorial = new Factorial();
        InvocationHandler handler = new InvocationHandlerImpl(factorial);
        IFactorial proxy = (IFactorial) Proxy.newProxyInstance(
                factorial.getClass().getClassLoader(), Factorial.class.getInterfaces(), handler);
        for (int i = 0; i < 12 ; i ++){
        logger.info(String.valueOf(proxy.getFactorial(i)));}
        logger.info(String.valueOf(proxy.getFactorial(5)));


    }
}
