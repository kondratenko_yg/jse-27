package ru.kondratenko.nlmk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Logger;

public class InvocationHandlerImpl implements InvocationHandler {

    public static final Logger logger = Logger.getLogger(InvocationHandlerImpl.class.getName());

    private Factorial factorial;

    final int MAX_CAPACITY = 10;
    Map<Integer, Integer> lruCache = new LinkedHashMap<>(MAX_CAPACITY, 0.75f, true) {
        @Override
        protected boolean removeEldestEntry(Map.Entry<Integer, Integer> eldest) {
            return size() > MAX_CAPACITY;
        }
    };

    public InvocationHandlerImpl(Factorial factorial) {
        this.factorial = factorial;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (method.getName().equals("getFactorial")) {
            Integer argument = (Integer) args[0];
            if (lruCache.containsKey(argument)) {
                logger.info("[NEXT VALUE FROM CACHE]");
                return lruCache.get(argument);
            } else {
                Integer result = (Integer) method.invoke(factorial, args);
                lruCache.put(argument, result);
                return result;
            }
        }
        return method.invoke(factorial, args);
    }

}
